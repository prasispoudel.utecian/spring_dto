package com.utechit.Student.Admission.Controller;
import com.utechit.Student.Admission.converter.StudentConverter;
import com.utechit.Student.Admission.dto.StudentDTO;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
// Importing Student Service
import com.utechit.Student.Admission.Service.StudentService;
// Importing data Student(table)
import com.utechit.Student.Admission.model.Student;
// Importing data StudentType(table)
import com.utechit.Student.Admission.model.StudentType;
@RestController
@RequestMapping("api/student")
//@CrossOrigin(origins="http://localhost:4200")
public class StudentAPIcontroller {
    @Autowired
    private StudentService studentService;
    @Autowired
    private StudentConverter converter;
      @Autowired
      public StudentAPIcontroller(StudentConverter converter,StudentService service){
        this.studentService=service;
          this.converter=converter;
      }
      @PostMapping("save")
      public StudentDTO addStudent(@RequestBody StudentDTO dto){
          Student student = converter.DTOtoEntity(dto);
          student = studentService.addStudent(student);
          return converter.entitytoDTO(student);
      }
    @GetMapping("/findAllStudents")
    public List<StudentDTO> findAll() {
        this.converter.map(new StudentDTO(), Student.class);
        return this.converter.DTOtoEntity(this.studentService.findAll());
    }
    @GetMapping("/find/{id}")
    public StudentDTO findById(@PathVariable(value = "id") Integer id) {
        Student orElse = studentService.findById(id);
        return converter.entitytoDTO(orElse);
    }
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable(value = "id") Integer id) {
        StudentDTO studentDTO = new StudentDTO();
        studentDTO.setId(id);
        Student student = converter.DTOtoEntity(studentDTO);
        studentService.deleteById(student.getId());
    }
    @PutMapping("/{id}")
    @ResponseBody
    public void update(@PathVariable("id") Integer id,@RequestBody StudentDTO dto){
        Student student = converter.DTOtoEntity(dto);
        this.studentService.updateStudent(id,student);
    }
}
