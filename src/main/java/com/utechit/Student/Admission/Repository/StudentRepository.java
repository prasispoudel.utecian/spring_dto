package com.utechit.Student.Admission.Repository;
import com.utechit.Student.Admission.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface StudentRepository extends CrudRepository<Student,  Integer> {
    List<Student> findByName(String Name);

}
