package com.utechit.Student.Admission.model;
// import java.persistence.*;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name="student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id",unique = true)
    private int id;
    @Column(name="name")
    private String name;
    @Column(name = "address")
    private String address;
    @ManyToOne
    private StudentType studentType;

    public StudentType getStudentType() {
        return studentType;
    }
    public void setStudentType(StudentType studentType) {
        this.studentType = studentType;
    }
    public Student(){

    }
    public Student(int id,String name,String address,StudentType studentType){
        this.id=id;
        this.name=name;
        this.address=address;
        this.studentType=studentType;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", studentType=" + studentType +
                '}';
    }
}
