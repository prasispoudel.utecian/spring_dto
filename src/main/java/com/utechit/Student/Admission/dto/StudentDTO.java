package com.utechit.Student.Admission.dto;
import com.utechit.Student.Admission.model.StudentType;
public class StudentDTO {
   private int id;
   private String name;
   private String address;
   private StudentType studentType;

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getAddress() {
      return address;
   }

   public void setAddress(String address) {
      this.address = address;
   }

   public StudentType getStudentType() {
      return studentType;
   }
   public void setStudentType(StudentType studentType) {
      this.studentType = studentType;
   }

}
