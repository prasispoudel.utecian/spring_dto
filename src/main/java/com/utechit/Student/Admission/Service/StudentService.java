package com.utechit.Student.Admission.Service;
import com.utechit.Student.Admission.Repository.StudentRepository;
import com.utechit.Student.Admission.model.Student;
import com.utechit.Student.Admission.model.StudentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    private StudentRepository studentRepository;
    @Autowired
    public StudentService(StudentRepository studentRepository){
        this.studentRepository=studentRepository;
    }
    public List<Student> getStudentList(){
        return (List<Student>) this.studentRepository.findAll();
    }
    public Student addStudent(Student student){
        return this.studentRepository.save(student);
    }
    public Student findById(Integer id){
        return this.studentRepository.findById(id).get();
    }
    public void deleteById(Integer id){
        this.studentRepository.deleteById(id);
    }
    public List<Student> findByName(String Name){
        return this.studentRepository.findByName(Name);
    }
    public List<Student> findAll() {
        return (List<Student>) this.studentRepository.findAll();
    }
    public void updateStudent(int id, Student student){
     Optional<Student> student1 =this.studentRepository.findById(id);
     String name = student1.get().getName();
     String address = student1.get().getAddress();
     StudentType studenttype=student1.get().getStudentType();
     name = student.getName();
     address = student.getAddress();
     studenttype = student.getStudentType();
     this.studentRepository.save(new Student(id,name,address,studenttype));
    }
}
