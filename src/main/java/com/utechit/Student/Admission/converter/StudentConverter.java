package com.utechit.Student.Admission.converter;

import com.utechit.Student.Admission.dto.StudentDTO;
import com.utechit.Student.Admission.model.Student;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class StudentConverter extends ModelMapper {

    public StudentDTO entitytoDTO(Student student){
        ModelMapper mapper = new ModelMapper();
        StudentDTO map = mapper.map(student,StudentDTO.class);
        return map;
    }
    public Student DTOtoEntity(StudentDTO studentDTO){
        ModelMapper mapper= new ModelMapper();
        Student map = mapper.map(studentDTO,Student.class);
        return map;
    }

    public List<Student>  entitytoDTO(List<StudentDTO> list) {
        return list.stream().map(dto -> super.map(dto, Student.class)).collect(Collectors.toList());
    }

    public List<StudentDTO> DTOtoEntity(List<Student> list) {
        return list.stream().map(entity -> super.map(entity, StudentDTO.class)).collect(Collectors.toList());
    }
}
